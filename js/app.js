'use strict';

// Variável que Controla a largura do tabuleiro
var larguraTabuleiro = 16;

// Variavel que controla a altura do tabuleiro
var alturaTabuleiro = 8;

// Gerando tabuleiro baseado na largura e altura definida
geraTabuleiro();

// Adicionando metodo compose para as functions
Function.prototype.compose  = function(argFunction) {
  var invokingFunction = this;
  return function() {
    return  invokingFunction.call(this,argFunction.apply(this,arguments));
  }
}

// Merge de array
// Permite que seja aplicado um filtro que elemina valores duplicados de um array
function onlyUnique(value, index, self) { 
  return self.indexOf(value) === index;
}

// O que uma ação pode fazer
// Pode ser adicionado para qualquer coisa que seja uma ação
var asAcao = function(){
  this.posicoesRedor = function(posicaoCorrente) {

    var posicoes = [];
    var encontrei = false;

    // Se a posição for 1 - Estou no cima esquerda
    if (posicaoCorrente == 1) {
      posicoes.push(posicaoCorrente+1);
      posicoes.push(posicaoCorrente+larguraTabuleiro);
      encontrei = true;
    };

    // Se a posição for igual a largura - Estou no cima direita
    if (posicaoCorrente == larguraTabuleiro) {
      posicoes.push(posicaoCorrente-1);
      posicoes.push(posicaoCorrente+(larguraTabuleiro-1));
      posicoes.push(posicaoCorrente+larguraTabuleiro);
      encontrei = true;
    };

    // Se a posição for igual a largura*altura-(larguraTabuleiro-1) - Estou no baixo esquerda
    if (posicaoCorrente == (larguraTabuleiro * alturaTabuleiro) - (alturaTabuleiro - 1)) {
      posicoes.push(posicaoCorrente-larguraTabuleiro);
      posicoes.push(posicaoCorrente-(larguraTabuleiro-1));
      posicoes.push(posicaoCorrente+1);
      encontrei = true;
    };

    // Se a posição for igual a largura*altura - Estou no baixo direita
    if (posicaoCorrente == (larguraTabuleiro * alturaTabuleiro)) {
      posicoes.push(posicaoCorrente-larguraTabuleiro);
      posicoes.push(posicaoCorrente-1);
      encontrei = true;
    };

    // > 1 && < larguraTabuleiro
    if (posicaoCorrente > 1 && posicaoCorrente < larguraTabuleiro) {
      if (posicaoCorrente % 2 == 0) {
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+1);
        posicoes.push(posicaoCorrente+(larguraTabuleiro-1));
        posicoes.push(posicaoCorrente+larguraTabuleiro);
        posicoes.push(posicaoCorrente+(larguraTabuleiro+1)); 
      } else {
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+1);
        posicoes.push(posicaoCorrente+larguraTabuleiro);
      };
      encontrei = true;
    };

    // Coluna esquerda
    var finalContador = larguraTabuleiro + 1;
    var contador = (larguraTabuleiro * alturaTabuleiro) - ((larguraTabuleiro + larguraTabuleiro) - 1);
    for (contador; contador >= finalContador ; contador -= larguraTabuleiro) {
      if(posicaoCorrente == contador){
        posicoes.push(posicaoCorrente-larguraTabuleiro);
        posicoes.push(posicaoCorrente-(larguraTabuleiro-1));
        posicoes.push(posicaoCorrente+1);
        posicoes.push(posicaoCorrente+larguraTabuleiro);
        encontrei = true;
      };
    };

    // Coluna direita
    var finalContador = larguraTabuleiro + larguraTabuleiro;
    var contador = (larguraTabuleiro * alturaTabuleiro) - larguraTabuleiro;
    for (contador; contador >= finalContador ; contador -= larguraTabuleiro) {
      if(posicaoCorrente == contador){
        posicoes.push(posicaoCorrente-larguraTabuleiro);
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+(larguraTabuleiro-1));
        posicoes.push(posicaoCorrente+larguraTabuleiro);
        encontrei = true;
      };
    };

    // > (larguraTabuleiro-1)1 && < larguraTabuleiro0
    if (posicaoCorrente > larguraTabuleiro * alturaTabuleiro - (larguraTabuleiro - 1) && posicaoCorrente < larguraTabuleiro * alturaTabuleiro) {
      if (posicaoCorrente % 2 == 0) {
        posicoes.push(posicaoCorrente-larguraTabuleiro);
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+1);
      } else {
        posicoes.push(posicaoCorrente-(larguraTabuleiro+1));
        posicoes.push(posicaoCorrente-larguraTabuleiro);
        posicoes.push(posicaoCorrente-(larguraTabuleiro-1));
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+1);
      };
      encontrei = true;
    };

    if (!encontrei) {
      if (posicaoCorrente % 2 == 0) {
        posicoes.push(posicaoCorrente-larguraTabuleiro);
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+1);
        posicoes.push(posicaoCorrente+(larguraTabuleiro-1));
        posicoes.push(posicaoCorrente+larguraTabuleiro);
        posicoes.push(posicaoCorrente+(larguraTabuleiro+1)); 
      } else {
        posicoes.push(posicaoCorrente-(larguraTabuleiro+1));
        posicoes.push(posicaoCorrente-larguraTabuleiro);
        posicoes.push(posicaoCorrente-(larguraTabuleiro-1));
        posicoes.push(posicaoCorrente-1);
        posicoes.push(posicaoCorrente+1);
        posicoes.push(posicaoCorrente+larguraTabuleiro);
      };
    };
    return posicoes;
  };

  this.calculaPosicoesPossiveis = function(posicao){
    // console.log('This: ', this);
    this.posicoesPossiveis = this.posicoesRedor(posicao);
    if (this.alcance > 1) {
      for (var i = this.alcance-2; i >= 0; i--) {
        for (var j = this.posicoesPossiveis.length - 1; j >= 0; j--) {
          this.posicoesPossiveis = this.posicoesPossiveis.concat(this.posicoesRedor(this.posicoesPossiveis[j]));
          this.posicoesPossiveis = this.posicoesPossiveis.filter(onlyUnique);
        };
      };
    };
  };

  this.marcaPosicoes = function(posicao){
    this.calculaPosicoesPossiveis(posicao);
    this.posicoesPossiveis.forEach(function (posicao) {
      $('.position'+posicao).addClass('posicao-possivel');
    });
  };

};




// Jogador
var Jogador = function() {
  console.info('Novo jogador criado.');

  this.nome = 'Player';
  this.id = 0;
  this.cor = '#fff'
  this.posicao = 0;

  this.nivel = 1;
  
  this.vida = 40;
  this.pontosMovimento = 3;
  this.pontosAcao = 5;

  this.forca = 1;
  this.vitalidade = 1;
  this.agilidade = 1;
  this.inteligencia = 1;
  this.destreza = 1;
  this.sorte = 1;

  // Metodo de um jogador que desmarca a posição corrente do jogador no mapa
  this.desmarcarPosicao = function(){
    $('.position'+this.posicao+' polygon').css('fill','');
    $('.position'+this.posicao+' path').css('fill','');
    console.info('Jogador '+this.nome+' foi foi desmarcado da posição '+this.posicao);
  };

  // Metodo de um jogador que marca a posição corrente do jogador no mapa usando sua cor
  this.marcarPosicao = function(){
    //console.log('this ', this);
    $('.position'+this.posicao+' polygon').css('fill',this.cor);
    $('.position'+this.posicao+' path').css('fill',this.corTime);
    console.info('Jogador '+this.nome+' foi foi marcado na posição '+this.posicao);
  };

  this.entrarTime = function(time){
    this.corTime = time.cor;
    time.jogadores.push(this.id);
    console.info('Jogador '+this.nome+' entrou no time '+time.nome);
  }

  return this;
};




// Base
var Base = function(posicao) {
  console.info('Nova base criada.');

  this.nome = 'Base';
  this.cor = '#fff';
  this.id = 0;
  this.posicao = posicao;

  this.nivel = 1;
  this.vida = 100;

  // Metodo de uma base que marca a posição corrente do base no mapa usando sua cor
  this.fixarBase = function(){
    //console.log('this ', this);
    $('.position'+this.posicao+' polygon').css('fill',this.cor);
    $('.position'+this.posicao).attr('id','base'+this.id);
    console.info('Base '+this.nome+' foi marcada na posição '+this.posicao);
  };

  this.entrarTime = function(time){
    this.cor = time.cor;
    time.baseId = this.id;
    console.info('Time '+time.nome+' agora tem a base '+this.nome);
  }

  return this;
};
//Base.compose();




var Time = function(){
  console.info('Novo time criado.');
  this.nome = 'Time';
  this.id = 0;
  this.cor = '#fff';
  this.baseId = 0;
  this.jogadores = [];
}

var time1 = new Time();
time1.nome = 'Bonzinhos';
time1.cor = '#9b59b6';
// time1.baseId = 1;
// time1.jogadores.push(1);

var time2 = new Time();
time2.nome = 'Malzões';
time2.cor = '#7f8c8d';
// time2.baseId = 2;
// time2.jogadores.push(2);




// Coisas que podem se mover
var podeSeMover = function(){

  var that = this;

  // Como uma highlight
  var highlightIrPara = function(){
    console.info('Highlightando alcance do Ir Para para o jogador '+ that.nome);
    this.marcaPosicoes(that.posicao);
  };

  

  this.movimento = {
    // Definição do alcance de um jogador
    // @TODO Aqui não pode ser alcance pois é muito vago. Os ataques também terão alcance
    alcance : 3,

    // Metodo que move um jogador
    irPara : function(novaPosicao){
      $('.posicao-possivel').removeClass('posicao-possivel');
      $('.position'+that.posicao).attr('id','');
      that.posicao = novaPosicao;
      $('.position'+that.posicao).attr('id','jogador'+that.id);
    },

    // Compondo um highlightIrPara
    highlightIrPara : highlightIrPara.compose(asAcao)
  }
}

// Compondo Jogador
var Jogador = Jogador.compose(podeSeMover);





// Startando Jogador 1
var jogador1 = new Jogador();
jogador1.nome = 'Jack';
jogador1.id = 1;
jogador1.cor = '#3498db';
jogador1.movimento.irPara(2);
jogador1.entrarTime(time1);
jogador1.marcarPosicao(); // marcarPosicao agora depois de entrar no time pois ganha a cor do time
// jogador1.movimento.highlightIrPara();

// Startando Jogador 2
var jogador2 = new Jogador();
jogador2.nome = 'John';
jogador2.id = 2;
jogador2.cor = '#f1c40f';
jogador2.movimento.irPara(127);
// Setando um alcance maior para o jogador 2, apenas para ilustrar
jogador2.movimento.alcance = 5;
jogador2.entrarTime(time2);
jogador2.marcarPosicao(); // marcarPosicao agora depois de entrar no time pois ganha a cor do time
//jogador2.movimento.highlightIrPara();

jogador2.desmarcarPosicao();
jogador2.movimento.irPara(3);
jogador2.marcarPosicao();

// Startando Base 1
var base1 = new Base(1);
base1.nome = 'Base 1';
base1.id = 1;
base1.cor = '#9b59b6';
base1.fixarBase();
base1.entrarTime(time1);

// Startando Base 2
var base2 = new Base(128);
base2.nome = 'Base 2';
base2.id = 2;
base2.cor = '#e74c3c';
base2.fixarBase();
base2.entrarTime(time2);