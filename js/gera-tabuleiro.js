'use strict'

//código para criar uma nova div 
function geraTabuleiro(){
  var html = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="60px" height="60px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve"> <g> <polygon fill="#2ECC71" points="15.308,60 0.559,30.5 15.308,1 44.692,1 59.441,30.5 44.692,60 "/> <path fill="#21B75D" d="M44.191,1l14.595,29L44.383,59H15.617L1.118,30L15.617,1H44 M45.001,0H14.999L0,30l14.999,30h30.002L60,30L45.001,0L45.001,0z"/> </g> </svg>';
  var i = 1;
  var meuTabuleiro = "<div class='tabuleiro'>";
  
  // for que vai criar cada linha do tabuleiro, cada linha é uma div com a classe hex-row.
  for(var a = 0; a < alturaTabuleiro; a++)
  {
    
	var divRow = "<div class='hex-row'>";
	var validador = true;

	// for que cria cada coluna (hexagono) do tabuleiro, cada coluna é uma div com a classe hex positionX aonde X é a posição dela.
    for(var l = 0; l < larguraTabuleiro; l++)
    {
      // if para saber se a posição da coluna é par (true) ou impar (false) cada div de coluna par recebe a clase even em adição as que ja possui.
      if(i%2 === 0)
      {
    	var hexPosition = 'hex position'.concat(i).concat(' even');
    	div = div.concat("<div class='").concat(hexPosition);
      }
      else
      {
    	var hexPosition = 'hex position'.concat(i);
    	
    	// if para saber se eu estou começando uma linha nova (true) ou continuando uma antiga (false).
    	if(validador)
    	{
    		var div = "<div class='";
    		validador = false;
    		div = div.concat(hexPosition);
    	}
    	else
    	{
    		div = div.concat("<div class='").concat(hexPosition);
    	}
      }
      div = div.concat("' posicao='"+ i +"' ><span class='label'>"+i+"</span>").concat(html).concat('</div>');

      i++;
    }
    divRow = divRow.concat(div).concat('</div>');
    meuTabuleiro = meuTabuleiro.concat(divRow);
  }
  meuTabuleiro = meuTabuleiro.concat('</div>');
  $('#container-master').append(meuTabuleiro);
}